/*
 *  Hidden: Source Stats - Death Location Reporter
 *
 *  Description:
 *    Logs player deaths to the log to be processed by the stats processor at a later date.
 *
 *  Changelog:
 *    v1.0.1
 *      Implemented death reporting for IRIS if hdn_deathnotices is set to 0.
 *    v1.0.0
 *      Initial release.
 *
 *  Contact:
 *    Hidden Source Forums: iammitch @ http://forum.hidden-source.com/
 *
*/

#include <sourcemod>
#include <sdktools>

// Defines
#define PLUGIN_VERSION "1.0.1"
#define TEAM_HIDDEN 3

public Plugin:myinfo = {
	name = "Hidden Stats : Death Position Recorder",
	author = "iammitch",
	description = "A plugin that records more information about a players death into the logfile for the Stats system to process.",
	version = PLUGIN_VERSION,
	url = "http://forum.hidden-source.com/showthread.php?11610-Stats-Revival"
};

new Handle:convarDeathNotices;
/**
	Event that is fired when the plugin is started.
	Parameters: None
	Returns: Nothing
*/
public OnPluginStart() {

	CreateConVar(
		"hsm_stats_kill_log_version",
		PLUGIN_VERSION,
		"Hidden: Source Stats Kill Logger Version",
		FCVAR_PLUGIN|FCVAR_SPONLY|FCVAR_NOTIFY
		);

	// We need to watch this CVar, since it's value determines how we determine if players have died.
	convarDeathNotices = FindConVar("hdn_deathnotices");

	// Hook to the events that are needed.
	HookEvent( "player_death", Event_PlayerDeath );
	HookEvent( "player_hurt", Event_PlayerHurt );

}

public Action:Event_PlayerHurt ( Handle:event, const String:name[], bool:dontBroadcast ) {

	// This event is ignored if the convar for death notices is set to 1.
	if ( GetConVarInt(convarDeathNotices) == 1 ) {
		return Plugin_Continue;
	}

	new victim = GetClientOfUserId(GetEventInt(event, "userid"));
	if ( victim == 0 ) {
		// Ignore BOT kills.
		return Plugin_Continue;
	}

	if ( GetClientTeam ( victim ) == TEAM_HIDDEN ) {
		return Plugin_Continue;
	}

	new health = GetClientHealth(victim);

	if ( health <= 0 ) {
		// IRIS player has been killed, log it.
		new attacker = GetClientOfUserId(GetEventInt(event, "attacker"));
		if ( attacker == 0 ) {
			// Ignore BOt kills.
			return Plugin_Continue;
		}
		PlayerKilled ( event, attacker, victim );
	}

	return Plugin_Continue;

}

public Action:Event_PlayerDeath ( Handle:event, const String:name[], bool:dontBroadcast ) {

	new attacker_id = GetClientOfUserId(GetEventInt(event, "attacker"));
	new victim_id = GetClientOfUserId(GetEventInt(event, "userid"));
	
	if ( attacker_id == 0 || attacker_id == victim_id ) {

		// Suicide/World
		new victim_team = GetClientTeam ( victim_id );

		decl String:weapon[32];
		GetEventString(event, "weapon", weapon, sizeof(weapon));

		decl String:victim_steamID[32];
		GetClientAuthString( victim_id, victim_steamID, sizeof(victim_steamID) );

		decl Float:VictimPos[3];
		GetClientEyePosition ( victim_id, VictimPos );

		decl String:map[32];
		GetCurrentMap(map, sizeof(map));

		LogToGame( "StatKill: <1>-<%s>-<%s>-<%s><%i><%f,%f,%f>", 
			map, 
			weapon,
			victim_steamID, victim_team, VictimPos[0], VictimPos[1], VictimPos[2] );

	}
	else {
		PlayerKilled ( event, attacker_id, victim_id );
	}

	return Plugin_Continue;

}

PlayerKilled ( Handle:event, attacker_id, victim_id ) {

	// PvP
	new attacker_team = GetClientTeam ( attacker_id );
	new victim_team = GetClientTeam ( victim_id );

	decl String:weapon[32];
	GetEventString(event, "weapon", weapon, sizeof(weapon));

	decl String:victim_steamID[32];
	GetClientAuthString( victim_id, victim_steamID, sizeof(victim_steamID) );

	decl String:attacker_steamID[32];
	GetClientAuthString( attacker_id, attacker_steamID, sizeof(attacker_steamID) );

	decl Float:AttackerPos[3];
	decl Float:VictimPos[3];

	GetClientEyePosition ( attacker_id, AttackerPos );
	GetClientEyePosition ( victim_id, VictimPos );

	decl String:map[32];
	GetCurrentMap(map, sizeof(map));

	LogToGame( "StatKill: <0>-<%s>-<%s><%i><%f,%f,%f>-<%s>-<%s><%i><%f,%f,%f>", 
		map, 
		attacker_steamID, attacker_team, AttackerPos[0], AttackerPos[1], AttackerPos[2], 
		weapon,
		victim_steamID, victim_team, VictimPos[0], VictimPos[1], VictimPos[2] );

}